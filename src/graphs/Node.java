/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author kingm14
 */
public class Node implements Comparable {

    private String name;
    private double lWeight;
    private Node prevNode;
    private Edge prevEdge;
    private double weightFromStart;
    private ArrayList<Edge> children;
    private Point position;
    private double vx;
    private double vy;
    private double ax;
    private double ay;
    private boolean visited;
    private boolean isLocked;

    public Node(String name) {
        visited = false;
        this.name = name;
        prevNode = null;
        weightFromStart = Double.MAX_VALUE;
        children = new ArrayList<Edge>();
        position = null;
        vx = 0;
        vy = 0;
        ax = 0;
        ay = 0;
    }

    public void setVisited(boolean b) {
        visited = b;
    }

    public boolean getVisited() {
        return visited;
    }

    public void addChild(Edge e) {
        children.add(e);
    }

    public ArrayList<Edge> getChildren() {
        return children;
    }

    public Node getPrevNode() {
        return prevNode;
    }

    public void setPrevNode(Node n) {
        prevNode = n;
    }
    public Edge getPrevEdge() {
        return prevEdge;
    }

    public void setPrevEdge(Edge n) {
        prevEdge = n;
    }

    public void setWFS(double d) {
        weightFromStart = d;
    }

    public double getWFS() {
        return weightFromStart;
    }

    public String getName() {
        return name;
    }

    public double getLWeight() {
        return lWeight;
    }

    public void setLWeight(double d) {
        lWeight = d;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point pos) {
        position = pos;
    }

    public void setVX(double d) {
        vx = d;
    }

    public double getVX() {
        return vx;
    }

    public void setVY(double d) {
        vy = d;
    }

    public double getVY() {
        return vy;
    }

    public void setAX(double d) {
        ax = d;
    }

    public double getAX() {
        return ax;
    }

    public void setAY(double d) {
        ay = d;
    }

    public double getAY() {
        return ay;
    }
    public void setIsLocked(boolean b)
    {
        isLocked=b;
    }
    public boolean getIsLocked()
    {
        return isLocked;
    }
    public Point update(double timeStep, double netForceX, double netForceY) {
        if(isLocked)
        {
            return new Point(0, 0);
        }
        double damping = .9;
        double pvx = vx;
        double pvy = vy;
        vx = ((vx + (netForceX * timeStep)) * damping);
        vy = ((vy + (netForceY * timeStep)) * damping);
        //System.out.println(getName()+": "+vx+","+ vy);
        Double VX = new Double(vx);
        Double VY = new Double(vy);
        if (VX.isNaN() || VX.isInfinite()) {
            vx = pvx;
        }
        if (VY.isNaN() || VY.isInfinite()) {
            vy = pvy;
        }
        /*System.out.println(name);
         System.out.println(" nfx:"+netForceX+" nfy:"+netForceY);
         System.out.println(" vx:"+vx+" vy:"+vy);
         */
        position.setLocation(position.getX() + ((timeStep * vx)), position.getY() + (timeStep * vy));
        int x = 0;
        int y = 0;
        /*if(position.getX()<20)
         {
         x=(int)(20-position.getX());
         }
         if(position.getY()<20)
         {
         y=(int)(20-position.getY());
         }
         if(position.getX()>1900)
         {
         x=(int)(-1*(position.getX()-1900));
         }
         if(position.getY()>1900)
         {
         y=(int)(-1*(position.getY()-1900));
         }*/
        return new Point(x, y);
        //System.out.println("moved");
    }

    public void shift(int sX, int sY) {
        position.translate(sX, sY);
    }

    public void rotate(int angle, Point center) {
        shift((int)(-1*center.getX()), (int)(-1*center.getY()));
        double newX;
        double newY;
        double theta = Math.toRadians(angle);
        Point oldP = getPosition();
        if (angle > 0) {
            newX = oldP.getX() * Math.cos(theta) + oldP.getY() * Math.sin(theta);
            newY = (-1 * oldP.getX() * Math.sin(theta)) + oldP.getY() * Math.cos(theta);
        } else {
            newX = oldP.getX() * Math.cos(theta) + oldP.getY() * Math.sin(theta);
            newY = (-1 * oldP.getX() * Math.sin(theta)) + oldP.getY() * Math.cos(theta);
        }
        position = new Point((int) newX,(int)newY);
        shift((int)(center.getX()), (int)(center.getY()));
    }
    public void rotate(double angle, Point center) {
        shift((int)(-1*center.getX()), (int)(-1*center.getY()));
        double newX;
        double newY;
        double theta = angle;
        Point oldP = getPosition();
        if (angle > 0) {
            newX = oldP.getX() * Math.cos(theta) + oldP.getY() * Math.sin(theta);
            newY = (-1 * oldP.getX() * Math.sin(theta)) + oldP.getY() * Math.cos(theta);
        } else {
            newX = oldP.getX() * Math.cos(theta) + oldP.getY() * Math.sin(theta);
            newY = (-1 * oldP.getX() * Math.sin(theta)) + oldP.getY() * Math.cos(theta);
        }
        position = new Point((int) newX,(int)newY);
        shift((int)(center.getX()), (int)(center.getY()));
    }

    public int compareTo(Object t) {
        return children.size() - ((Node) t).getChildren().size();
    }
}
