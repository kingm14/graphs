/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

/**
 *
 * @author kingm14
 */
import javax.swing.JApplet;

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Applet2 extends JApplet implements KeyListener, MouseMotionListener, MouseListener, MouseWheelListener {

    /**
     * Initializes the applet Applet2
     */
    private String fileName = "us-edges-big.txt";
    //private final String fileName = "graph1.txt";
    private double scale = 1;
    private boolean isUSMap = true;
    private final boolean tryFancy = false;
    private Point pressedLoc;
    ArrayList<Node> gList;
    private double total_ke;
    private double prevKE;
    private double minKE;
    private int timeStep = 50;
    private HashMap<Point, Node> used;
    private Random r;
    private int minX;
    private int minY;
    Scanner keyboard = new Scanner(System.in);
    private double pow;
    private boolean spacePressed;
    private boolean redraw;
    private boolean cont = true;
    private int clickingNodes;
    private GraphAlgorithms ga;
    private Node sNode;
    private Node eNode;
    private boolean drawPath;
    private boolean drawMinTree;
    private int sleepAmount;
    private Point center;
    private ArrayList<Edge> usedEdges;
    private boolean dragNodeEnabled;
    private Node draggingNode;
    private double zoomVal;
    private int screenSizeX;
    private int screenSizeY;
    @Override
    public void init() {
        super.setSize(1920, 1000);
        // TODO start asynchronous download of heavy resources
        screenSizeX=Math.abs((int)super.getSize().getWidth())/2;
        screenSizeY=Math.abs((int)super.getSize().getHeight())/2;
        zoomVal = 1;
        dragNodeEnabled = false;
        drawMinTree = false;
        usedEdges = new ArrayList<Edge>();
        sleepAmount = 1;
        drawPath = false;
        clickingNodes = -1;
        pressedLoc = new Point();
        redraw = true;
        addKeyListener(this);
        addMouseMotionListener(this);
        addMouseListener(this);
        addMouseWheelListener(this);
        gList = new ArrayList<Node>();
        ga = new GraphAlgorithms();

        ga.readFromFile(fileName, scale);

        gList = ga.getAllNodes();
        total_ke = 0;
        used = new HashMap<Point, Node>();
        
        r = new Random();
        spacePressed = true;
        pow = 1;
        prevKE = Integer.MAX_VALUE - 500;
        minKE = Integer.MAX_VALUE;
        /*Scanner keyboard=new Scanner(System.in);
         System.out.print("Please enter start point: ");
         String s=keyboard.nextLine();
         while(!ga.validNode(s))
         {
         System.out.print("  Invalid input, Please enter start point: ");
         s=keyboard.nextLine();
         }
         System.out.println();
         System.out.print("Please enter end point: ");
         String e=keyboard.nextLine();
         while(!ga.validNode(e))
         {
         System.out.print("  Invalid input, Please enter end point: ");
         e=keyboard.nextLine();
         }*/
        //ga.shortenEdges(s, e);

        //MapDrawer m=new MapDrawer();
        initGraph(ga.getAllNodes());
        //repaint();
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Applet2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Applet2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Applet2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Applet2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the applet */
        try {
            java.awt.EventQueue.invokeAndWait(new Runnable() {
                public void run() {
                    initComponents();
                    filePathBox.setText(fileName);
                    springKSlider.setValue(100);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void reinit() {
        sleepAmount = 1;
        drawPath = false;
        clickingNodes = -1;
        pressedLoc = new Point();
        redraw = true;
        //scale=slider.getValue();
        gList = new ArrayList<Node>();
        ga = new GraphAlgorithms();

        ga.readFromFile(fileName, scale);

        gList = ga.getAllNodes();
        total_ke = 0;
        used = new HashMap<Point, Node>();

        r = new Random();
        spacePressed = true;
        pow = 1;
        prevKE = Integer.MAX_VALUE - 500;
        minKE = Integer.MAX_VALUE;
        /*Scanner keyboard=new Scanner(System.in);
         System.out.print("Please enter start point: ");
         String s=keyboard.nextLine();
         while(!ga.validNode(s))
         {
         System.out.print("  Invalid input, Please enter start point: ");
         s=keyboard.nextLine();
         }
         System.out.println();
         System.out.print("Please enter end point: ");
         String e=keyboard.nextLine();
         while(!ga.validNode(e))
         {
         System.out.print("  Invalid input, Please enter end point: ");
         e=keyboard.nextLine();
         }*/
        //ga.shortenEdges(s, e);

        //MapDrawer m=new MapDrawer();
        initGraph(ga.getAllNodes());
        //repaint();
    }

    public boolean ccw(Point a, Point b, Point c) {
        return ((c.getY() - a.getY()) * (b.getX() - a.getX())) > ((b.getY() - a.getY()) * (c.getX() - a.getX()));
    }

    public boolean doesIntersect(Point a, Point b, Point c, Point d) {
        if (a == null || b == null || c == null || d == null) {
            return false;
        }
        return ccw(a, c, d) != ccw(b, c, d) && ccw(a, b, c) != ccw(a, b, d);
    }

    public int getAllIntersections() {
        int i = 0;
        for (Node n : gList) {
            ArrayList<Edge> k = n.getChildren();
            for (Edge e : k) {
                i += getNumIntersections(n.getPosition(), e.getEnd().getPosition());
            }
        }
        return i;
    }

    public int getNumIntersections(Point start, Point end) {
        int inters = 0;
        for (Node temp : gList) {
            ArrayList<Edge> kids = temp.getChildren();
            for (Edge edge : kids) {
                if (doesIntersect(start, end, edge.getStart().getPosition(), edge.getEnd().getPosition())) {
                    inters++;
                }
            }

        }
        //inters /= 2;
        //System.out.println("  " + inters);
        return inters;
    }

    public void initKids(Node node) {
        ArrayList<Edge> kids = node.getChildren();
        for (Edge tempE : kids) {
            if (tempE.getEnd().getPosition() == null) {
                if (tryFancy) {
                    double bestAngle = 0;
                    double minInters = Integer.MAX_VALUE;
                    for (double tempangle = 10; tempangle <= 360; tempangle += (360 / node.getChildren().size())) {
                        double angle = Math.toRadians(tempangle);
                        Point tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                        while (used.containsKey(tempP)) {
                            tempangle += (360 / node.getChildren().size());
                            angle = Math.toRadians(tempangle);
                            tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                        }
                        int inters = getNumIntersections(tempP, node.getPosition());
                        if (inters <= minInters) {
                            bestAngle = angle;
                            minInters = inters;
                        }
                    }
                    Point tempP = new Point((int) (node.getPosition().getX() + (Math.cos(bestAngle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(bestAngle) * tempE.getWeight())));
                    tempE.getEnd().setPosition(tempP);
                    initKids(tempE.getEnd());

                } else {
                    double angle = Math.toRadians(r.nextInt(360));
                    Point tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                    while (used.containsKey(tempP)) {
                        angle = Math.toRadians(r.nextInt(360));
                        tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                    }
                    tempE.getEnd().setPosition(tempP);
                    initKids(tempE.getEnd());
                }
            }
        }
    }

    public void initGraph(ArrayList<Node> gListT) {
        gList = gListT;
        Collections.sort(gList);
        ArrayList<Edge> edges = new ArrayList<Edge>();

        for (int i = 0; i < gList.size(); i++) {
            Node node = gList.get(i);
            ArrayList<Edge> kids = node.getChildren();
            if (node.getPosition() == null) {
                Point p = new Point(r.nextInt(screenSizeX) , r.nextInt(screenSizeY));
                while (used.containsKey(p)) {
                    p = new Point(r.nextInt(screenSizeX), r.nextInt(screenSizeY));
                }
                node.setPosition(p);
                initKids(node);
            }

            used.put(node.getPosition(), node);

            /*for(Edge tempE: kids)
             {
             if(tempE.getEnd().getPosition()==null)
             {
             double angle=Math.toRadians(r.nextInt(360));
             Point  tempP=new Point((int)(node.getPosition().getX()+(Math.cos(angle)*tempE.getWeight())), (int)(node.getPosition().getY()+(Math.sin(angle)*tempE.getWeight())));
             while(used.containsKey(tempP))
             {
             angle=Math.toRadians(r.nextInt(360));
             tempP=new Point((int)(node.getPosition().getX()+(Math.cos(angle)*tempE.getWeight())), (int)(node.getPosition().getY()+(Math.sin(angle)*tempE.getWeight())));
             }
             tempE.getEnd().setPosition(tempP);
             }
             }*/
            node.setAX(0);
            node.setAY(0);
            node.setVX(0);
            node.setVY(0);
            edges.addAll(node.getChildren());

        }
        minX = (int) gList.get(0).getPosition().getX();
        minY = (int) gList.get(0).getPosition().getY();
        for (int i = 1; i < gList.size(); i++) {
            Point temp = gList.get(i).getPosition();
            if (temp.getX() < minX) {
                minX = (int) temp.getX();
            }
            if (temp.getY() < minY) {
                minY = (int) temp.getY();
            }
        }
        /*do
         {
         updateGraph();
         repaint();
         try {
         Thread.sleep(timeStep);
         } catch (InterruptedException ex) {
         Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
         }
         }while(total_ke>5);*/
        repaint();
        //System.out.println();
    }

    public void paint(Graphics g) {


        //System.out.println("  " + total_ke);

        //System.out.println(g.getFont().getSize());
        updateZoomVal();
        zoomLabel.setText((int) (zoomVal * 100) + "%");
        if (total_ke != 0 && total_ke <= minKE) {
            //System.out.print(minKE+"v");
            minKE = total_ke;
            //System.out.println(minKE);
            cont = false;

        }

        int scaleFactor = 1;
        if (scaleFactor < 0) {
            scaleFactor = -1 / scaleFactor;
        }
        g.clearRect(0, 0, screenSizeX, screenSizeY);
        super.paint(g);
        g.setColor(Color.black);
        //g.drawString("" + getQuality(), 10, 10);
        //g.fillOval(10, 10, 10, 10);
        initGraph(gList);
        Point p = new Point();
        Random r = new Random();
        //g.setFont(new Font(g.getFont().getName(),Font.PLAIN, 10));
        for (int i = 0; i < gList.size(); i++) {
            if (drawPath && gList.get(i).getVisited() || drawMinTree) {
                g.setColor(Color.RED);
            } else if (drawPath) {
                g.setColor(Color.LIGHT_GRAY);
            } else {
                g.setColor(Color.black);
            }
            //if(scale>.5)
            g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, (int) (fontSlider.getValue())));
            /*else
             g.setFont(new Font(g.getFont().getFontName(),Font.PLAIN,(int)(5)));*/
            //System.out.println(gList.get(i).getPosition().getX());
            if (gList.get(i).getIsLocked()) {
                Color bc = g.getColor();
                g.setColor(Color.DARK_GRAY);

                g.fillOval(((int) (gList.get(i).getPosition().getX() - 7)), (((int) (gList.get(i).getPosition().getY()) - 7)), 14, 14);
                g.setColor(bc);
            }
            g.drawOval(((int) (gList.get(i).getPosition().getX() - 7)), (((int) (gList.get(i).getPosition().getY()) - 7)), 14, 14);
            g.drawString(gList.get(i).getName(), (((int) (gList.get(i).getPosition().getX()) - 10)), ((int) (gList.get(i).getPosition().getY())));
            ArrayList<Edge> children = gList.get(i).getChildren();
            for (Edge e : children) {
                if (!e.getColor().equals(Color.white)) {
                    if (drawPath && (e.beenVisited() || (e.getStart().getVisited() && e.getEnd().getVisited()))) {
                        g.setColor(Color.RED);
                    } else if (drawPath || drawMinTree) {
                        g.setColor(Color.LIGHT_GRAY);
                    } else {
                        g.setColor(e.getColor());
                    }
                    if (isUSMap) {
                        g.drawString("" + e.getName(), (int) (e.getStart().getPosition().getX() + e.getEnd().getPosition().getX()) / 2, (int) (e.getStart().getPosition().getY() + e.getEnd().getPosition().getY()) / 2);
                    } else {


                        g.drawString("" + ((int) e.getUnscaledWeight()), (int) (e.getStart().getPosition().getX() + e.getEnd().getPosition().getX()) / 2, (int) (e.getStart().getPosition().getY() + e.getEnd().getPosition().getY()) / 2);
                    }
                    //System.out.println("Pref: "+e.getWeight()+" Actual: "+e.getLength());
                    g.drawLine((int) e.getStart().getPosition().getX(), (int) e.getStart().getPosition().getY(), (int) e.getEnd().getPosition().getX(), (int) e.getEnd().getPosition().getY());

                }
            }
            if (drawMinTree) {
                g.setColor(Color.RED);
                for (Edge e : usedEdges) {
                    g.drawString("" + ((int) e.getUnscaledWeight()), (int) (e.getStart().getPosition().getX() + e.getEnd().getPosition().getX()) / 2, (int) (e.getStart().getPosition().getY() + e.getEnd().getPosition().getY()) / 2);
                    //System.out.println("Pref: "+e.getWeight()+" Actual: "+e.getLength());
                    g.drawLine((int) e.getStart().getPosition().getX(), (int) e.getStart().getPosition().getY(), (int) e.getEnd().getPosition().getX(), (int) e.getEnd().getPosition().getY());
                }
            }

        }
        //System.out.println(total_ke);
        /*if(!cont)
         {
         try {
         Thread.sleep(1000);
         } catch (InterruptedException ex) {
         Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
         }
         cont=true;
         }*/
        //sleepAmount=timeSlider.getValue();
        if (total_ke == 0 || total_ke > 1.5) {

            if (spacePressed) {

                try {
                    Thread.sleep(sleepAmount);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
                }

                updateGraph();
                //System.out.println(total_ke);
            }

        }
        drawButtons(g);
        //update(g);







    }

    /*public void update(Graphics g)
     {
    
     System.out.println("hi");
     super.update(g);
     updateGraph();
     }*/
    public double getQuality() {
        double qual = 0;
        for (Node n : gList) {
            ArrayList<Edge> kids = n.getChildren();
            for (Edge e : kids) {
                qual += e.getLength() - e.getWeight();
            }
        }

        //qual += getAllIntersections();
        return (qual / scale)/*+ getAllIntersections()*/;
    }

    public void updateGraph() {
        pow = powSlider.getValue();
        //powLabel.setText("= " + pow);
        double damping = 0.5;
        int shiftX = 0;
        int shiftY = 0;
        double time = 1;
        total_ke = 0;
        for (int i = 0; i < gList.size(); i++) {
            Node node = gList.get(i);

            //repaint();
            /*try {
             Thread.sleep(5);
             } catch (InterruptedException ex) {
             Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
             }*/
            double netForceX = 0;
            double netForceY = 0;
            for (int k = 0; k < gList.size(); k++) {

                Point me = node.getPosition();
                Point other = gList.get(k).getPosition();
                if (i != k) {
                    double angle = 0;
                    double y = 0;
                    double x = 0;
                    if (other.getY() > me.getY()) {
                        y = other.getY() - me.getY();

                    } else {
                        y = me.getY() - other.getY();

                    }
                    if (other.getX() > me.getX()) {
                        x = other.getX() - me.getX();

                    } else {
                        x = me.getX() - other.getX();

                    }
                    angle = Math.atan(y / x);
                    double dist = me.distance(other) / scale;
                    double prevQual = getQuality();
                    double curQual = prevQual;


                    if (other.getX() > me.getX()) {
                        netForceX -= (pow) * (Math.cos(angle) * (1 / (dist * dist)));
                    } else {
                        netForceX += (pow) * (Math.cos(angle) * (1 / (dist * dist)));
                    }
                    if (other.getY() > me.getY()) {
                        netForceY -= (pow) * (Math.sin(angle) * (1 / (dist * dist)));
                    } else {
                        netForceY += (pow) * (Math.sin(angle) * (1 / (dist * dist)));
                    }

                }
            }

            ArrayList<Edge> edges = node.getChildren();
            double k = springKSlider.getValue() / 100.0;
            //System.out.println(k);
            //System.out.println(node.getName());
            for (Edge e : edges) {
                /*System.out.print(" "+e.getEnd().getName()+" angle:"+Math.toDegrees(e.getAngle()));
                 if(e.isUpsideDown())
                 {
                 System.out.print(" S");
                 }
                 else
                 {
                 System.out.print(" N");
                 }
                
                 if(e.isBackward())
                 {
                 System.out.print("W ");
                 }
                 else
                 {
                 System.out.print("E ");
                 }
                 System.out.println(": nfx:"+e.getAttX()+" nfy:"+e.getAttY());*/
                netForceX += e.getAttX(k);
                netForceY += e.getAttY(k);
                //e.printError();
            }
            //System.out.println(" total: x:"+netForceX+" y:"+netForceY);

            node.setAX(netForceX);
            node.setAY(netForceY);

            Point temp = node.update(.5, netForceX, netForceY);
            if (Math.abs(temp.getX()) > Math.abs(shiftX)) {
                shiftX = 0;
            }
            if (Math.abs(temp.getY()) > Math.abs(shiftY)) {
                shiftY = 0;
            }

            total_ke = total_ke + (Math.sqrt(node.getVX() * node.getVX() + node.getVY() * node.getVY()));
            //System.out.println(total_ke);
            //repaint();
        }
        if (total_ke != 0) {
            time = 1 / (total_ke / 200);
            //System.out.println(time+" s");
        }
        if (shiftX != 0 || shiftY != 0) {
            for (int j = 0; j < gList.size(); j++) {
                gList.get(j).shift(shiftX, shiftY);
            }
        }
        HashMap<Point, Node> map = new HashMap<Point, Node>();
        for (Node temp : gList) {
            Point tempP = temp.getPosition();
            while (map.containsKey(tempP)) {
                System.out.println("singularity at " + temp.getName());
                temp.shift(1, 1);
                tempP.translate(1, 1);
            }
            map.put(tempP, temp);
        }
        /*if(total_ke<=minKE)
         {
         repaint();
         minKE=total_ke;
         }
         else if(total_ke<Integer.MAX_VALUE-1)
         {
         updateGraph();
         }*/
        repaint();

    }

    public Node getClickedNode(Point p) {
        for (Node cur : gList) {
            if (cur.getPosition().distance(p) < 10) {
                //System.out.println(cur.getName());
                return cur;
            }
        }
        /*for(int xVal=(int)p.getX()-4; xVal<=p.getX()-4; xVal++)
         {
         for(int yVal=(int)p.getY()-4; yVal<=p.getY()-4; yVal++)
         {
         Point tempP= new Point(xVal,yVal);
         if(used.containsKey(tempP))
         {
         return used.get(tempP);
         }
         }
         }*/
        return null;
    }

    public void getMinSpanTree() {
        ArrayList<Node> nodeList = new ArrayList<Node>();
        usedEdges = ga.getMinSpanningTree(gList);
        int totalW = 0;
        for (int i = 0; i < usedEdges.size(); i++) {
            totalW += usedEdges.get(i).getUnscaledWeight();
        }
        //System.out.println(totalW);
        treeLengthLabel.setText("= " + totalW);
        /* nodeList.add(al.get(0).getStart());
         for (Edge tEdge : al) {
         nodeList.add(tEdge.getEnd());
         }
         for (Node tNode : gList) {
         ArrayList<Edge> k=tNode.getChildren();
            
         for(Edge e: k)
         {
         for(Edge temp: al)
         {
         if(temp.equals(e))
         e.setVisited(true);
         }
                
         }
            
         }*/
        //System.out.print("");
    }

    public void clearNodes() {
        for (Node n : gList) {
            n.setVisited(false);
        }
    }
    // TODO overwrite start(), stop() and destroy() methods

    public void keyTyped(KeyEvent ke) {
        if (ke.getKeyChar() == '1') {
            pow++;
            //System.out.println(pow);
        } else if (ke.getKeyChar() == '2') {
            pow--;
            //System.out.println(pow);
        }
    }

    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyChar() == 'c') {
            spacePressed = false;
        } else if (ke.getKeyChar() == '9') {
            drawMinTree = true;
            clearNodes();
            getMinSpanTree();
        } else if (ke.getKeyChar() == '0') {
            drawMinTree = false;
            clickingNodes = -1;
        } else if (ke.getKeyChar() == 'r') {
            init();
        } else if (ke.getKeyChar() == 'p') {
            cont = true;
        } else if (ke.getKeyChar() == 'i') {
            for (int i = 0; i < gList.size(); i++) {

                ArrayList<Edge> children = gList.get(i).getChildren();
                for (Edge e : children) {
                    e.printError();
                }

            }
        } else if (ke.getKeyChar() == '+') {
            for (Node temp : gList) {
                ArrayList<Edge> children = temp.getChildren();
                temp.setPosition(new Point((int) ((temp.getPosition().getX() - screenSizeX) * 1.5) + screenSizeX, (int) ((temp.getPosition().getY() - screenSizeY) * 1.5) + screenSizeY));
                for (Edge e : children) {
                    e.setWeight(e.getWeight() * (1.5));
                }
            }
        } else if (ke.getKeyChar() == '-') {
            for (Node temp : gList) {
                temp.setPosition(new Point((int) ((temp.getPosition().getX() - screenSizeX) / 1.5) + screenSizeX, (int) ((temp.getPosition().getY() - screenSizeY) / 1.5) + screenSizeY));
                ArrayList<Edge> children = temp.getChildren();
                for (Edge e : children) {
                    e.setWeight(e.getWeight() / (1.5));
                }
            }
        }



    }

    public void keyReleased(KeyEvent ke) {
        if (ke.getKeyChar() == 'c') {
            spacePressed = true;
        }

    }

    @Override
    public void mouseDragged(MouseEvent me) {
        if (rotateButton.isSelected()) {

            double myAngle = Math.atan((me.getPoint().getY() - screenSizeY) / (me.getPoint().getX() - screenSizeX));
            double sAngle = Math.atan((pressedLoc.getY() - screenSizeY) / (pressedLoc.getX() - screenSizeX));
            //System.out.println("initial: "+Math.toDegrees(sAngle)+" "+Math.toDegrees(myAngle));
            for (Node temp : gList) {
                temp.rotate(-1 * (myAngle - sAngle), new Point(screenSizeX, screenSizeY));
            }
            pressedLoc = me.getPoint();
        } else {
            if (dragNodeEnabled && draggingNode != null) {
                draggingNode.setPosition(me.getPoint());
            } else {
                Point tempP = me.getLocationOnScreen();

                if (pressedLoc.distance(tempP) > 1) {
                    int transX = (int) (tempP.getX() - pressedLoc.getX());
                    int transY = (int) (tempP.getY() - pressedLoc.getY());

                    for (int i = 0; i < gList.size(); i++) {
                        gList.get(i).shift(transX, transY);
                    }

                }
                pressedLoc = tempP;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
        center = me.getPoint();
    }

    public void drawButtons(Graphics g) {
        /*Font font = g.getFont();
         Color color = g.getColor();
         g.setColor(Color.black);
         g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 13));
         g.drawRect(10, 10, 50, 20);
         g.drawString("redraw", 15, 25);
         if (drawPath) {
         g.setColor(Color.blue);
         }
         g.drawRect(10, 40, 120, 20);
         g.drawString("Get Shortest Path", 15, 55);
        
         g.setColor(Color.black);
         if(drawMinTree)
         {
         g.setColor(Color.blue);
         }
         g.drawRect(10, 70, 150, 20);
         g.drawString("Get Min Spanning Tree", 15, 85);
         g.setFont(font);
         g.setColor(color);*/
    }

    public void getPath(Node startNode, Node endNode) {
        
        //System.out.println("step 2");
        
        if (endNode != null && startNode != null && !endNode.equals(startNode)) {
             DirectionsTextPane.setText("Begin at "+startNode.getName()+"\n");
            ga.shortenEdges(endNode.getName(), startNode.getName());
            //ga.getDirections(sNode.getName(), eNode.getName());
            ArrayList<Node> nodeList = new ArrayList<Node>();
            ArrayList<Edge> al = ga.getPath(endNode.getName(), startNode.getName());
            nodeList.add(al.get(0).getStart());
            for (Edge tEdge : al) {
                nodeList.add(tEdge.getEnd());
                String next=("   -continue on "+tEdge.getName() + " for " + Math.round(tEdge.getUnscaledWeight())+" miles\nArrive at "+tEdge.getStart().getName());
                DirectionsTextPane.setText(DirectionsTextPane.getText()+next+"\n");
            }
            for (Node tNode : gList) {
                for (Node tNode2 : nodeList) {
                    if (tNode.getName().equals(tNode2.getName())) {
                        tNode.setVisited(true);
                    }
                }
            }
            endNode.setVisited(true);
            startNode.setVisited(true);
            //ga.getDirections(sNode.getName(), eNode.getName());
        }
    }

    public void mousePressed(MouseEvent me) {

        if (dragNodeEnabled) {
            draggingNode = getClickedNode(me.getPoint());
        }

        pressedLoc = me.getLocationOnScreen();
        if (drawPath) {
            if (me.getButton() == MouseEvent.BUTTON1) {
                for (Node n : gList) {
                    n.setVisited(false);
                }
                //System.out.println("step 2");
                Node pNode = sNode;
                sNode = getClickedNode(me.getPoint());
                if (sNode != null) {
                    sNode.setVisited(true);
                } else {
                    sNode = pNode;
                }
                /*if (sNode != null && eNode != null && !sNode.equals(eNode)) {

                    ga.shortenEdges(sNode.getName(), eNode.getName());
                    //ga.getDirections(sNode.getName(), eNode.getName());
                    ArrayList<Node> nodeList = new ArrayList<Node>();
                    ArrayList<Edge> al = ga.getPath(sNode.getName(), eNode.getName());
                    nodeList.add(al.get(0).getStart());
                    for (Edge tEdge : al) {
                        nodeList.add(tEdge.getEnd());
                        System.out.println(tEdge.getName() + " for " + tEdge.getUnscaledWeight());
                    }
                    for (Node tNode : gList) {
                        for (Node tNode2 : nodeList) {
                            if (tNode.getName().equals(tNode2.getName())) {
                                tNode.setVisited(true);
                            }
                        }
                    }
                    sNode.setVisited(true);
                    eNode.setVisited(true);
                    //ga.getDirections(sNode.getName(), eNode.getName());


                }*/
                
                getPath(sNode,eNode);
                
            } else if (me.getButton() == MouseEvent.BUTTON3) {
                for (Node n : gList) {
                    n.setVisited(false);
                }
                //System.out.println("step 2");
                Node pNode = eNode;
                eNode = getClickedNode(me.getPoint());
                if (eNode != null) {
                    eNode.setVisited(true);
                } else {
                    eNode = pNode;
                }
                /*if (sNode != null && eNode != null && !sNode.equals(eNode)) {
                    clickingNodes = -1;
                    ga.shortenEdges(sNode.getName(), eNode.getName());
                    //ga.getDirections(sNode.getName(), eNode.getName());
                    ArrayList<Node> nodeList = new ArrayList<Node>();
                    ArrayList<Edge> al = ga.getPath(sNode.getName(), eNode.getName());
                    nodeList.add(al.get(0).getStart());
                    for (Edge tEdge : al) {
                        nodeList.add(tEdge.getEnd());
                        System.out.println(tEdge.getName() + " for " + tEdge.getUnscaledWeight());
                    }
                    for (Node tNode : gList) {
                        for (Node tNode2 : nodeList) {
                            if (tNode.getName().equals(tNode2.getName())) {
                                tNode.setVisited(true);
                            }
                        }
                    }
                    sNode.setVisited(true);
                    eNode.setVisited(true);
                }*/
                getPath(sNode,eNode);
                //ga.getDirections(sNode.getName(), eNode.getName());
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        draggingNode = null;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void zoomIn(double zoomFactor, Point myCenter) {

        //int zsv=zoomSlider.getValue();


        scale *= zoomFactor;

        for (Node temp : gList) {
            ArrayList<Edge> children = temp.getChildren();
            temp.setPosition(new Point((int) ((temp.getPosition().getX() - (int) myCenter.getX()) * zoomFactor) + (int) myCenter.getX(), (int) ((temp.getPosition().getY() - (int) myCenter.getY()) * zoomFactor) + (int) myCenter.getY()));
            for (Edge e : children) {
                e.setWeight(e.getWeight() * (zoomFactor));
                e.setScale(e.getScale() * zoomFactor);
            }
        }

    }

    public void zoomOut(double zoomFactor, Point myCenter) {
        scale /= zoomFactor;
        for (Node temp : gList) {
            temp.setPosition(new Point((int) ((temp.getPosition().getX() - (int) myCenter.getX()) / zoomFactor) + (int) myCenter.getX(), (int) ((temp.getPosition().getY() - (int) myCenter.getY()) / zoomFactor) + (int) myCenter.getY()));
            ArrayList<Edge> children = temp.getChildren();
            for (Edge e : children) {
                e.setWeight(e.getWeight() / (zoomFactor));
                e.setScale(e.getScale() / zoomFactor);
            }
        }
    }

    public void updateZoomVal() {
        if (scale > 1) {
            zoomVal = (scale - 1);

        } else if (scale < 1) {
            zoomVal = ((-1 / scale) + 1);

        } else {
            zoomVal = (0);
        }
    }
    //@Override

    public void mouseWheelMoved(MouseWheelEvent mwe) {
        //System.out.println(mwe.getWheelRotation());
        //System.out.println();
        if (mwe.isShiftDown()) {
            //System.out.println(sleepAmount + "+" + mwe.getUnitsToScroll());
            sleepAmount += mwe.getUnitsToScroll();
            if (sleepAmount < 1) {
                sleepAmount = 1;
            }
        } else {

            double zoomFactor = 1.1;
            //int zsv=zoomSlider.getValue();
            if (mwe.getWheelRotation() < 0) {

                zoomIn(zoomFactor, center);
            } else {
                zoomOut(zoomFactor, center);
            }


        }
    }

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    public void mouseClicked(MouseEvent me) {
        if (lockButton.isSelected()) {
            Node node = getClickedNode(me.getPoint());
            if (node.getIsLocked()) {
                node.setIsLocked(false);
            } else {
                node.setIsLocked(true);
            }
        }
        //throw new UnsupportedOperationException("Not supported yet.");
        /*Point point = me.getPoint();
         if (point.getX() >= 10 && point.getY() >= 40 && point.getX() <= 130 && point.getY() <= 60) {
         drawMinTree=false;
         if (drawPath) {
         drawPath = false;
         } else {
         drawPath = true;
         }
         } else if (point.getX() >= 10 && point.getY() >= 10 && point.getX() <= 60 && point.getY() <= 30) {
         reinit();
         }
         else if(point.getX() >= 10 && point.getY() >= 70 && point.getX() <= 160 && point.getY() <= 90)
         {
         drawPath = false;
         clearNodes();
         if(!drawMinTree)
         {
         drawMinTree=true;

         getMinSpanTree();
         }
         else
         {
         drawMinTree=false;
         }
         }*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        filePathBox = new javax.swing.JTextField();
        jToggleButton1 = new javax.swing.JToggleButton();
        powSlider = new javax.swing.JSlider();
        treeLengthLabel = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        zoomLabel = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        rotateButton = new javax.swing.JToggleButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        fontSlider = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        springKSlider = new javax.swing.JSlider();
        lockButton = new javax.swing.JToggleButton();
        namesButton = new javax.swing.JToggleButton();
        dirFrame = new javax.swing.JInternalFrame();
        jScrollPane1 = new javax.swing.JScrollPane();
        DirectionsTextPane = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        sDirButton = new javax.swing.JToggleButton();

        jButton1.setText("Redraw");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Shortest Path");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Min Spanning Tree");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Set File");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        filePathBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filePathBoxActionPerformed(evt);
            }
        });

        jToggleButton1.setText("Drag Nodes");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        powSlider.setMaximum(30000000);
        powSlider.setMinimum(10);
        powSlider.setOrientation(javax.swing.JSlider.VERTICAL);

        treeLengthLabel.setText("= ?");

        jButton5.setText("-");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        zoomLabel.setText("jLabel1");

        jButton6.setText("+");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        rotateButton.setText("Rotate");
        rotateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rotateButtonActionPerformed(evt);
            }
        });

        jButton7.setText("Flip Y axis");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText("Flip X Axis");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        fontSlider.setMaximum(50);
        fontSlider.setMinimum(1);
        fontSlider.setValue(12);

        jLabel1.setText("Font");

        jLabel2.setText("EM Force");

        jLabel3.setText("Spring");

        jLabel4.setText("Strength");

        springKSlider.setMaximum(215);
        springKSlider.setMinimum(100);
        springKSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        springKSlider.setSnapToTicks(true);

        lockButton.setText("Lock Nodes");
        lockButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lockButtonActionPerformed(evt);
            }
        });

        namesButton.setSelected(true);
        namesButton.setText("Show Road Names");
        namesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namesButtonActionPerformed(evt);
            }
        });

        dirFrame.setVisible(true);

        DirectionsTextPane.setColumns(20);
        DirectionsTextPane.setRows(5);
        jScrollPane1.setViewportView(DirectionsTextPane);

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel5.setText("Directions");

        org.jdesktop.layout.GroupLayout dirFrameLayout = new org.jdesktop.layout.GroupLayout(dirFrame.getContentPane());
        dirFrame.getContentPane().setLayout(dirFrameLayout);
        dirFrameLayout.setHorizontalGroup(
            dirFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(dirFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(jScrollPane1)
                .addContainerGap())
            .add(dirFrameLayout.createSequentialGroup()
                .add(59, 59, 59)
                .add(jLabel5)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        dirFrameLayout.setVerticalGroup(
            dirFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, dirFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel5)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 12, Short.MAX_VALUE)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 330, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        sDirButton.setText("Hide Directions");
        sDirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sDirButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jLabel1))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(zoomLabel)
                                        .add(3, 3, 3)
                                        .add(jButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(fontSlider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(13, 13, 13)
                                        .add(jLabel2))
                                    .add(layout.createSequentialGroup()
                                        .add(23, 23, 23)
                                        .add(powSlider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                                    .add(springKSlider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jLabel3)
                                    .add(jLabel4)))))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(layout.createSequentialGroup()
                            .addContainerGap()
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(layout.createSequentialGroup()
                                    .add(jButton1)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(namesButton))
                                .add(layout.createSequentialGroup()
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jToggleButton1)
                                        .add(lockButton)
                                        .add(rotateButton)
                                        .add(layout.createSequentialGroup()
                                            .add(jButton7)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(jButton8))
                                        .add(layout.createSequentialGroup()
                                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(jButton3)
                                                .add(jButton2))
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(sDirButton)
                                                .add(treeLengthLabel))))
                                    .add(0, 0, Short.MAX_VALUE))))
                        .add(layout.createSequentialGroup()
                            .add(11, 11, 11)
                            .add(filePathBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 215, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jButton4))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 248, Short.MAX_VALUE)
                .add(dirFrame, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton4)
                            .add(filePathBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton1)
                            .add(namesButton))
                        .add(6, 6, 6)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton2)
                            .add(sDirButton))
                        .add(6, 6, 6)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton3)
                            .add(treeLengthLabel))
                        .add(7, 7, 7)
                        .add(jToggleButton1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lockButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(rotateButton)
                        .add(7, 7, 7)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton7)
                            .add(jButton8))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(zoomLabel)
                            .add(jButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                            .add(fontSlider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel1))
                        .add(17, 17, 17)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                            .add(layout.createSequentialGroup()
                                .add(jLabel3)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel4))
                            .add(jLabel2))
                        .add(12, 12, 12)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                            .add(springKSlider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(powSlider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(dirFrame, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(65, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        reinit();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        drawMinTree = false;
        if (drawPath) {
            drawPath = false;
        } else {
            drawPath = true;
        }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        drawPath = false;
        clearNodes();
        if (!drawMinTree) {
            drawMinTree = true;

            getMinSpanTree();
        } else {
            drawMinTree = false;
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void filePathBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filePathBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filePathBoxActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        fileName = filePathBox.getText();
        reinit();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        // TODO add your handling code here:
        if (dragNodeEnabled) {
            dragNodeEnabled = false;
        } else {
            dragNodeEnabled = true;
        }
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        zoomOut(1.2, new Point(960, 500));
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        zoomIn(1.2, new Point(960, 500));
    }//GEN-LAST:event_jButton6ActionPerformed

    private void rotateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rotateButtonActionPerformed
        // TODO add your handling code here:
        /*for(Node temp: gList)
         {
         temp.rotate(45,new Point(960,500));
         }*/
    }//GEN-LAST:event_rotateButtonActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        for (Node temp : gList) {
            temp.setPosition(new Point((int) (-1 * (temp.getPosition().getX() - 960)) + 960, (int) (temp.getPosition().getY())));
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        for (Node temp : gList) {
            temp.setPosition(new Point((int) ((temp.getPosition().getX())), (int) (-1 * (temp.getPosition().getY() - 500) + 500)));
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void lockButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lockButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lockButtonActionPerformed

    private void namesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namesButtonActionPerformed
        // TODO add your handling code here:
        if (namesButton.isSelected()) {
            isUSMap = true;
        } else {
            isUSMap = false;
        }
    }//GEN-LAST:event_namesButtonActionPerformed

    private void sDirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sDirButtonActionPerformed
        // TODO add your handling code here:
        if(sDirButton.isSelected())
        {
            sDirButton.setText("Show Directions");
            dirFrame.setVisible(false);
        }
        else
        {
            sDirButton.setText("Hide Directions");
            dirFrame.setVisible(true);
        }
    }//GEN-LAST:event_sDirButtonActionPerformed

    /*public static void main(String[] args)
    {
        Applet2 applet=new Applet2();
        applet.start();
    }*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea DirectionsTextPane;
    private javax.swing.JInternalFrame dirFrame;
    private javax.swing.JTextField filePathBox;
    private javax.swing.JSlider fontSlider;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton lockButton;
    private javax.swing.JToggleButton namesButton;
    private javax.swing.JSlider powSlider;
    private javax.swing.JToggleButton rotateButton;
    private javax.swing.JToggleButton sDirButton;
    private javax.swing.JSlider springKSlider;
    private javax.swing.JLabel treeLengthLabel;
    private javax.swing.JLabel zoomLabel;
    // End of variables declaration//GEN-END:variables
}
