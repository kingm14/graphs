/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

import javax.swing.JApplet;

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MapDrawer extends JApplet implements KeyListener, MouseMotionListener, MouseListener, MouseWheelListener {

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    private final String fileName = "us-edges-big.txt";
    //private final String fileName = "graph1.txt";
    private final double scale = 1;
    private final boolean isUSMap = false;
    private final boolean tryFancy = false;
    private Point pressedLoc;
    ArrayList<Node> gList;
    private double total_ke;
    private double prevKE;
    private double minKE;
    private int timeStep = 50;
    private HashMap<Point, Node> used;
    private Random r;
    private int minX;
    private int minY;
    Scanner keyboard = new Scanner(System.in);
    private double pow;
    private boolean spacePressed;
    private boolean redraw;
    private boolean cont = true;
    private int clickingNodes;
    private GraphAlgorithms ga;
    private Node sNode;
    private Node eNode;
    private boolean drawPath;
    private boolean drawMinTree;
    private int sleepAmount;
    private Point center;
    private ArrayList<Edge> usedEdges;

    public void init() {
        // TODO start asynchronous download of heavy resources
        drawMinTree=false;
        usedEdges=new ArrayList<Edge>();
        sleepAmount = 1;
        drawPath = false;
        clickingNodes = -1;
        pressedLoc = new Point();
        redraw = true;
        addKeyListener(this);
        addMouseMotionListener(this);
        addMouseListener(this);
        addMouseWheelListener(this);
        gList = new ArrayList<Node>();
        ga = new GraphAlgorithms();
        if (isUSMap) {
            ga.readFromFileL2L(fileName, scale);
        } else {
            ga.readFromFile(fileName, scale);
        }
        gList = ga.getAllNodes();
        total_ke = 0;
        used = new HashMap<Point, Node>();
        super.setSize(1920, 1000);
        r = new Random();
        spacePressed = true;
        pow = 1;
        prevKE = Integer.MAX_VALUE - 500;
        minKE = Integer.MAX_VALUE;
        /*Scanner keyboard=new Scanner(System.in);
        System.out.print("Please enter start point: ");
        String s=keyboard.nextLine();
        while(!ga.validNode(s))
        {
        System.out.print("  Invalid input, Please enter start point: ");
        s=keyboard.nextLine();
        }
        System.out.println();
        System.out.print("Please enter end point: ");
        String e=keyboard.nextLine();
        while(!ga.validNode(e))
        {
        System.out.print("  Invalid input, Please enter end point: ");
        e=keyboard.nextLine();
        }*/
        //ga.shortenEdges(s, e);

        //MapDrawer m=new MapDrawer();
        initGraph(ga.getAllNodes());
        //repaint();
    }

    public void reinit() {
        sleepAmount = 1;
        drawPath = false;
        clickingNodes = -1;
        pressedLoc = new Point();
        redraw = true;

        gList = new ArrayList<Node>();
        ga = new GraphAlgorithms();
        if (isUSMap) {
            ga.readFromFileL2L(fileName, scale);
        } else {
            ga.readFromFile(fileName, scale);
        }
        gList = ga.getAllNodes();
        total_ke = 0;
        used = new HashMap<Point, Node>();

        r = new Random();
        spacePressed = true;
        pow = 1;
        prevKE = Integer.MAX_VALUE - 500;
        minKE = Integer.MAX_VALUE;
        /*Scanner keyboard=new Scanner(System.in);
        System.out.print("Please enter start point: ");
        String s=keyboard.nextLine();
        while(!ga.validNode(s))
        {
        System.out.print("  Invalid input, Please enter start point: ");
        s=keyboard.nextLine();
        }
        System.out.println();
        System.out.print("Please enter end point: ");
        String e=keyboard.nextLine();
        while(!ga.validNode(e))
        {
        System.out.print("  Invalid input, Please enter end point: ");
        e=keyboard.nextLine();
        }*/
        //ga.shortenEdges(s, e);

        //MapDrawer m=new MapDrawer();
        initGraph(ga.getAllNodes());
        //repaint();
    }

    public boolean ccw(Point a, Point b, Point c) {
        return ((c.getY() - a.getY()) * (b.getX() - a.getX())) > ((b.getY() - a.getY()) * (c.getX() - a.getX()));
    }

    public boolean doesIntersect(Point a, Point b, Point c, Point d) {
        if(a==null|| b==null|| c==null|| d==null)
        {
            return false;
        }
        return ccw(a, c, d) != ccw(b, c, d) && ccw(a, b, c) != ccw(a, b, d);
    }

    public int getAllIntersections()
    {
        int i = 0;
        for (Node n : gList) {
            ArrayList<Edge> k = n.getChildren();
            for (Edge e : k) {
                i += getNumIntersections(n.getPosition(), e.getEnd().getPosition());
            }
        }
        return i;
    }
    
    public int getNumIntersections(Point start, Point end) {
        int inters = 0;
        for (Node temp : gList) {
            ArrayList<Edge> kids = temp.getChildren();
            for (Edge edge : kids) {
                if (doesIntersect(start, end, edge.getStart().getPosition(), edge.getEnd().getPosition())) {
                    inters++;
                }
            }

        }
        //inters /= 2;
        System.out.println("  "+inters);
        return inters;
    }

    public void initKids(Node node) {
        ArrayList<Edge> kids = node.getChildren();
        for (Edge tempE : kids) {
            if (tempE.getEnd().getPosition() == null) {
                if (tryFancy) {
                    double bestAngle=0;
                    double minInters=Integer.MAX_VALUE;
                    for (double tempangle = 10; tempangle <= 360; tempangle += (360/node.getChildren().size())) {
                        double angle=Math.toRadians(tempangle);
                        Point tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                        while (used.containsKey(tempP)) {
                            tempangle +=(360/node.getChildren().size());
                            angle=Math.toRadians(tempangle);
                            tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                        }
                        int inters=getNumIntersections(tempP,node.getPosition());
                        if(inters<=minInters)
                        {
                            bestAngle=angle;
                            minInters=inters;
                        }
                    }
                     Point tempP = new Point((int) (node.getPosition().getX() + (Math.cos(bestAngle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(bestAngle) * tempE.getWeight())));
                    tempE.getEnd().setPosition(tempP);
                    initKids(tempE.getEnd());
                    
                } else {
                    double angle = Math.toRadians(r.nextInt(360));
                    Point tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                    while (used.containsKey(tempP)) {
                        angle = Math.toRadians(r.nextInt(360));
                        tempP = new Point((int) (node.getPosition().getX() + (Math.cos(angle) * tempE.getWeight())), (int) (node.getPosition().getY() + (Math.sin(angle) * tempE.getWeight())));
                    }
                    tempE.getEnd().setPosition(tempP);
                    initKids(tempE.getEnd());
                }
            }
        }
    }

    public void initGraph(ArrayList<Node> gListT) {
        gList = gListT;
        Collections.sort(gList);
        ArrayList<Edge> edges = new ArrayList<Edge>();

        for (int i = 0; i < gList.size(); i++) {
            Node node = gList.get(i);
            ArrayList<Edge> kids = node.getChildren();
            if (node.getPosition() == null) {
                Point p = new Point(r.nextInt(500) + 100, r.nextInt(500) + 100);
                while (used.containsKey(p)) {
                    p = new Point(r.nextInt(500) + 100, r.nextInt(500) + 100);
                }
                node.setPosition(p);
                initKids(node);
            }

            used.put(node.getPosition(), node);

            /*for(Edge tempE: kids)
            {
            if(tempE.getEnd().getPosition()==null)
            {
            double angle=Math.toRadians(r.nextInt(360));
            Point  tempP=new Point((int)(node.getPosition().getX()+(Math.cos(angle)*tempE.getWeight())), (int)(node.getPosition().getY()+(Math.sin(angle)*tempE.getWeight())));
            while(used.containsKey(tempP))
            {
            angle=Math.toRadians(r.nextInt(360));
            tempP=new Point((int)(node.getPosition().getX()+(Math.cos(angle)*tempE.getWeight())), (int)(node.getPosition().getY()+(Math.sin(angle)*tempE.getWeight())));
            }
            tempE.getEnd().setPosition(tempP);
            }
            }*/
            node.setAX(0);
            node.setAY(0);
            node.setVX(0);
            node.setVY(0);
            edges.addAll(node.getChildren());

        }
        minX = (int) gList.get(0).getPosition().getX();
        minY = (int) gList.get(0).getPosition().getY();
        for (int i = 1; i < gList.size(); i++) {
            Point temp = gList.get(i).getPosition();
            if (temp.getX() < minX) {
                minX = (int) temp.getX();
            }
            if (temp.getY() < minY) {
                minY = (int) temp.getY();
            }
        }
        /*do
        {
        updateGraph();
        repaint();
        try {
        Thread.sleep(timeStep);
        } catch (InterruptedException ex) {
        Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
        }
        }while(total_ke>5);*/
        repaint();
        //System.out.println();
    }

    public void paint(Graphics g) {
        //System.out.println("  " + total_ke);

        //System.out.println(g.getFont().getSize());
        if (total_ke != 0 && total_ke <= minKE) {
            //System.out.print(minKE+"v");
            minKE = total_ke;
            //System.out.println(minKE);
            cont = false;

        }

        int scaleFactor = 1;
        g.clearRect(0, 0, 10000, 10000);
        g.setColor(Color.black);
        g.drawString("" + getQuality(), 10, 10);
        //g.fillOval(10, 10, 10, 10);
        initGraph(gList);
        Point p = new Point();
        Random r = new Random();
        //g.setFont(new Font(g.getFont().getName(),Font.PLAIN, 10));
        for (int i = 0; i < gList.size(); i++) {
            if (drawPath && gList.get(i).getVisited()||drawMinTree) {
                g.setColor(Color.RED);
            } else if (drawPath) {
                g.setColor(Color.LIGHT_GRAY);
            } else {
                g.setColor(Color.black);
            }

            //System.out.println(gList.get(i).getPosition().getX());
            g.drawOval(((int) (gList.get(i).getPosition().getX() - 7)), (((int) (gList.get(i).getPosition().getY()) - 7)), 14, 14);
            g.drawString(gList.get(i).getName(), (((int) (gList.get(i).getPosition().getX()) - 10)), ((int) (gList.get(i).getPosition().getY())));
            ArrayList<Edge> children = gList.get(i).getChildren();
            for (Edge e : children) {
                if (!e.getColor().equals(Color.white)) {
                    if (drawPath && (e.beenVisited() || (e.getStart().getVisited() && e.getEnd().getVisited()))) {
                        g.setColor(Color.RED);
                    } else if (drawPath||drawMinTree) {
                        g.setColor(Color.LIGHT_GRAY);
                    } else {
                        g.setColor(e.getColor());
                    }
                    g.drawString("" + ((int) e.getUnscaledWeight()), (int) (e.getStart().getPosition().getX() + e.getEnd().getPosition().getX()) / 2, (int) (e.getStart().getPosition().getY() + e.getEnd().getPosition().getY()) / 2);
                    //System.out.println("Pref: "+e.getWeight()+" Actual: "+e.getLength());
                    g.drawLine((int) e.getStart().getPosition().getX(), (int) e.getStart().getPosition().getY(), (int) e.getEnd().getPosition().getX(), (int) e.getEnd().getPosition().getY());

                }
            }
            if(drawMinTree)
            {
                g.setColor(Color.RED);
                for(Edge e:usedEdges)
                {
                    g.drawString("" + ((int) e.getUnscaledWeight()), (int) (e.getStart().getPosition().getX() + e.getEnd().getPosition().getX()) / 2, (int) (e.getStart().getPosition().getY() + e.getEnd().getPosition().getY()) / 2);
                    //System.out.println("Pref: "+e.getWeight()+" Actual: "+e.getLength());
                    g.drawLine((int) e.getStart().getPosition().getX(), (int) e.getStart().getPosition().getY(), (int) e.getEnd().getPosition().getX(), (int) e.getEnd().getPosition().getY());
                }
            }

        }
        //System.out.println(total_ke);
        /*if(!cont)
        {
        try {
        Thread.sleep(1000);
        } catch (InterruptedException ex) {
        Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
        }
        cont=true;
        }*/
        if (total_ke == 0 || total_ke > 1.5) {

            if (spacePressed) {

                try {
                    Thread.sleep(sleepAmount);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
                }

                updateGraph();
                //System.out.println(total_ke);
            }

        }
        drawButtons(g);
        //update(g);







    }

    /*public void update(Graphics g)
    {
    
    System.out.println("hi");
    super.update(g);
    updateGraph();
    }*/
    public double getQuality() {
        double qual = 0;
        for (Node n : gList) {
            ArrayList<Edge> kids = n.getChildren();
            for (Edge e : kids) {
                qual += e.getLength() - e.getWeight();
            }
        }
        
        //qual += getAllIntersections();
        return (qual / scale)/*+ getAllIntersections()*/;
    }

    public void updateGraph() {

        double damping = 0.5;
        int shiftX = 0;
        int shiftY = 0;
        double time = 1;
        total_ke = 0;
        for (int i = 0; i < gList.size(); i++) {
            Node node = gList.get(i);

            //repaint();
            /*try {
            Thread.sleep(5);
            } catch (InterruptedException ex) {
            Logger.getLogger(MapDrawer.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            double netForceX = 0;
            double netForceY = 0;
            for (int k = 0; k < gList.size(); k++) {

                Point me = node.getPosition();
                Point other = gList.get(k).getPosition();
                if (i != k) {
                    double angle = 0;
                    double y = 0;
                    double x = 0;
                    if (other.getY() > me.getY()) {
                        y = other.getY() - me.getY();

                    } else {
                        y = me.getY() - other.getY();

                    }
                    if (other.getX() > me.getX()) {
                        x = other.getX() - me.getX();

                    } else {
                        x = me.getX() - other.getX();

                    }
                    angle = Math.atan(y / x);
                    double dist = me.distance(other);
                    double prevQual = getQuality();
                    double curQual = prevQual;


                    if (other.getX() > me.getX()) {
                        netForceX -= (9 * Math.pow(10, pow)) * (Math.cos(angle) * (1 / (dist * dist)));
                    } else {
                        netForceX += (9 * Math.pow(10, pow)) * (Math.cos(angle) * (1 / (dist * dist)));
                    }
                    if (other.getY() > me.getY()) {
                        netForceY -= (9 * Math.pow(10, pow)) * (Math.sin(angle) * (1 / (dist * dist)));
                    } else {
                        netForceY += (9 * Math.pow(10, pow)) * (Math.sin(angle) * (1 / (dist * dist)));
                    }

                }
            }

            ArrayList<Edge> edges = node.getChildren();
            //System.out.println(node.getName());
            for (Edge e : edges) {
                /*System.out.print(" "+e.getEnd().getName()+" angle:"+Math.toDegrees(e.getAngle()));
                if(e.isUpsideDown())
                {
                System.out.print(" S");
                }
                else
                {
                System.out.print(" N");
                }
                
                if(e.isBackward())
                {
                System.out.print("W ");
                }
                else
                {
                System.out.print("E ");
                }
                System.out.println(": nfx:"+e.getAttX()+" nfy:"+e.getAttY());*/
                netForceX += e.getAttX();
                netForceY += e.getAttY();
                //e.printError();
            }
            //System.out.println(" total: x:"+netForceX+" y:"+netForceY);

            node.setAX(netForceX);
            node.setAY(netForceY);

            Point temp = node.update(.5, netForceX, netForceY);
            if (Math.abs(temp.getX()) > Math.abs(shiftX)) {
                shiftX = (int) temp.getX();
            }
            if (Math.abs(temp.getY()) > Math.abs(shiftY)) {
                shiftY = (int) temp.getY();
            }

            total_ke = total_ke + (Math.sqrt(node.getVX() * node.getVX() + node.getVY() * node.getVY()));
            //System.out.println(total_ke);
            //repaint();
        }
        if (total_ke != 0) {
            time = 1 / (total_ke / 200);
            //System.out.println(time+" s");
        }
        if (shiftX != 0 || shiftY != 0) {
            for (int j = 0; j < gList.size(); j++) {
                gList.get(j).shift(shiftX, shiftY);
            }
        }

        /*if(total_ke<=minKE)
        {
        repaint();
        minKE=total_ke;
        }
        else if(total_ke<Integer.MAX_VALUE-1)
        {
        updateGraph();
        }*/
        repaint();

    }

    public Node getClickedNode(Point p) {
        for (Node cur : gList) {
            if (cur.getPosition().distance(p) < 10) {
                //System.out.println(cur.getName());
                return cur;
            }
        }
        /*for(int xVal=(int)p.getX()-4; xVal<=p.getX()-4; xVal++)
        {
        for(int yVal=(int)p.getY()-4; yVal<=p.getY()-4; yVal++)
        {
        Point tempP= new Point(xVal,yVal);
        if(used.containsKey(tempP))
        {
        return used.get(tempP);
        }
        }
        }*/
        return null;
    }

    public void getMinSpanTree() {
        ArrayList<Node> nodeList = new ArrayList<Node>();
        usedEdges = ga.getMinSpanningTree(gList);

       /* nodeList.add(al.get(0).getStart());
        for (Edge tEdge : al) {
            nodeList.add(tEdge.getEnd());
        }
        for (Node tNode : gList) {
            ArrayList<Edge> k=tNode.getChildren();
            
            for(Edge e: k)
            {
                for(Edge temp: al)
                {
                    if(temp.equals(e))
                        e.setVisited(true);
                }
                
            }
            
        }*/
        System.out.print("");
    }

    public void clearNodes() {
        for (Node n : gList) {
            n.setVisited(false);
        }
    }
    // TODO overwrite start(), stop() and destroy() methods

    public void keyTyped(KeyEvent ke) {
        if (ke.getKeyChar() == '1') {
            pow++;
            System.out.println(pow);
        } else if (ke.getKeyChar() == '2') {
            pow--;
            System.out.println(pow);
        }
    }

    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyChar() == 'c') {
            spacePressed = false;
        } else if (ke.getKeyChar() == '9') {
            drawMinTree=true;
            clearNodes();
            getMinSpanTree();
        } else if (ke.getKeyChar() == '0') {
            drawMinTree = false;
            clickingNodes = -1;
        } else if (ke.getKeyChar() == 'r') {
            init();
        } else if (ke.getKeyChar() == 'p') {
            cont = true;
        } else if (ke.getKeyChar() == 'i') {
            for (int i = 0; i < gList.size(); i++) {

                ArrayList<Edge> children = gList.get(i).getChildren();
                for (Edge e : children) {
                    e.printError();
                }

            }
        } else if (ke.getKeyChar() == '+') {
            for (Node temp : gList) {
                ArrayList<Edge> children = temp.getChildren();
                temp.setPosition(new Point((int) ((temp.getPosition().getX() - 960) * 1.5) + 960, (int) ((temp.getPosition().getY() - 500) * 1.5) + 500));
                for (Edge e : children) {
                    e.setWeight(e.getWeight() * (1.5));
                }
            }
        } else if (ke.getKeyChar() == '-') {
            for (Node temp : gList) {
                temp.setPosition(new Point((int) ((temp.getPosition().getX() - 960) / 1.5) + 960, (int) ((temp.getPosition().getY() - 500) / 1.5) + 500));
                ArrayList<Edge> children = temp.getChildren();
                for (Edge e : children) {
                    e.setWeight(e.getWeight() / (1.5));
                }
            }
        }



    }

    public void keyReleased(KeyEvent ke) {
        if (ke.getKeyChar() == 'c') {
            spacePressed = true;
        }

    }

    @Override
    public void mouseDragged(MouseEvent me) {
        Point tempP = me.getLocationOnScreen();

        if (pressedLoc.distance(tempP) > 1) {
            int transX = (int) (tempP.getX() - pressedLoc.getX());
            int transY = (int) (tempP.getY() - pressedLoc.getY());

            for (int i = 0; i < gList.size(); i++) {
                gList.get(i).shift(transX, transY);
            }
        }
        pressedLoc = tempP;
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
        center=me.getPoint();
    }

    public void mouseClicked(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
        Point point = me.getPoint();
        if (point.getX() >= 10 && point.getY() >= 40 && point.getX() <= 130 && point.getY() <= 60) {
            drawMinTree=false;
            if (drawPath) {
                drawPath = false;
            } else {
                drawPath = true;
            }
        } else if (point.getX() >= 10 && point.getY() >= 10 && point.getX() <= 60 && point.getY() <= 30) {
            reinit();
        }
        else if(point.getX() >= 10 && point.getY() >= 70 && point.getX() <= 160 && point.getY() <= 90)
        {
            drawPath = false;
            clearNodes();
            if(!drawMinTree)
            {
                drawMinTree=true;

                getMinSpanTree();
    }
            else
            {
                drawMinTree=false;
            }
        }
    }

    public void drawButtons(Graphics g) {
        Font font = g.getFont();
        Color color = g.getColor();
        g.setColor(Color.black);
        g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 13));
        g.drawRect(10, 10, 50, 20);
        g.drawString("redraw", 15, 25);
        if (drawPath) {
            g.setColor(Color.blue);
        }
        g.drawRect(10, 40, 120, 20);
        g.drawString("Get Shortest Path", 15, 55);
        
        g.setColor(Color.black);
        if(drawMinTree)
        {
            g.setColor(Color.blue);
        }
        g.drawRect(10, 70, 150, 20);
        g.drawString("Get Min Spanning Tree", 15, 85);
        g.setFont(font);
        g.setColor(color);
    }

    public void mousePressed(MouseEvent me) {

        pressedLoc = me.getLocationOnScreen();
        if (drawPath) {
            if (me.getButton() == MouseEvent.BUTTON1) {
                for (Node n : gList) {
                    n.setVisited(false);
                }
                //System.out.println("step 2");
                Node pNode = sNode;
                sNode = getClickedNode(me.getPoint());
                if (sNode != null) {
                    sNode.setVisited(true);
                } else {
                    sNode = pNode;
                }
                if (sNode != null && eNode != null && !sNode.equals(eNode)) {

                    ga.shortenEdges(sNode.getName(), eNode.getName());
                    //ga.getDirections(sNode.getName(), eNode.getName());
                    ArrayList<Node> nodeList = new ArrayList<Node>();
                    ArrayList<Edge> al = ga.getPath(sNode.getName(), eNode.getName());
                    nodeList.add(al.get(0).getStart());
                    for (Edge tEdge : al) {
                        nodeList.add(tEdge.getEnd());
                    }
                    for (Node tNode : gList) {
                        for (Node tNode2 : nodeList) {
                            if (tNode.getName().equals(tNode2.getName())) {
                                tNode.setVisited(true);
                            }
                        }
                    }
                    sNode.setVisited(true);
                    eNode.setVisited(true);


                }
            } else if (me.getButton() == MouseEvent.BUTTON3) {
                for (Node n : gList) {
                    n.setVisited(false);
                }
                //System.out.println("step 2");
                Node pNode = eNode;
                eNode = getClickedNode(me.getPoint());
                if (eNode != null) {
                    eNode.setVisited(true);
                } else {
                    eNode = pNode;
                }
                if (sNode != null && eNode != null && !sNode.equals(eNode)) {
                    clickingNodes = -1;
                    ga.shortenEdges(sNode.getName(), eNode.getName());
                    //ga.getDirections(sNode.getName(), eNode.getName());
                    ArrayList<Node> nodeList = new ArrayList<Node>();
                    ArrayList<Edge> al = ga.getPath(sNode.getName(), eNode.getName());
                    nodeList.add(al.get(0).getStart());
                    for (Edge tEdge : al) {
                        nodeList.add(tEdge.getEnd());
                    }
                    for (Node tNode : gList) {
                        for (Node tNode2 : nodeList) {
                            if (tNode.getName().equals(tNode2.getName())) {
                                tNode.setVisited(true);
                            }
                        }
                    }
                    sNode.setVisited(true);
                    eNode.setVisited(true);
                }

            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    //@Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        //System.out.println(mwe.getWheelRotation());
        //System.out.println();
        if (mwe.isShiftDown()) {
            System.out.println(sleepAmount + "+" + mwe.getUnitsToScroll());
            sleepAmount += mwe.getUnitsToScroll();
            if (sleepAmount < 1) {
                sleepAmount = 1;
            }
        } else {
            double zoomFactor = 1.1;
            if (mwe.getWheelRotation() < 0) {
                for (Node temp : gList) {
                    ArrayList<Edge> children = temp.getChildren();
                    temp.setPosition(new Point((int) ((temp.getPosition().getX() - (int)center.getX()) * zoomFactor) + (int)center.getX(), (int) ((temp.getPosition().getY() - (int)center.getY()) * zoomFactor) + (int)center.getY()));
                    for (Edge e : children) {
                        e.setWeight(e.getWeight() * (zoomFactor));
                        e.setScale(e.getScale() * zoomFactor);
                    }
                }
            } else {
                for (Node temp : gList) {
                    temp.setPosition(new Point((int) ((temp.getPosition().getX() - 960) / zoomFactor) + 960, (int) ((temp.getPosition().getY() - 500) / zoomFactor) + 500));
                    ArrayList<Edge> children = temp.getChildren();
                    for (Edge e : children) {
                        e.setWeight(e.getWeight() / (zoomFactor));
                        e.setScale(e.getScale() / zoomFactor);
                    }
                }
            }
        }
    }
}
