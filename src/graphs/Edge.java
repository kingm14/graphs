/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

import java.awt.Color;
import java.awt.Point;
import java.util.Random;

/**
 *
 * @author kingm14
 */
public class Edge implements Comparable{
    private double weight;
    private Node start;
    private Node end;
    private boolean visited;
    private double attX;
    private double attY;
    private boolean upsideDown;
    private boolean backwards;
    private double scaleFactor;
    private Color myColor;
    private String name;
    private double initialWeight;
    public Edge(Node s, Node e, double w)
    {
        weight=w;
        start=s;
        end=e;
         upsideDown=false;
         backwards =false;
         this.scaleFactor=1;
         initialWeight=w;
         
         
    }
    public void setName(String s)
    {
        name=s;
    }
    public String getName()
    {
        return name;
    }
    public void setVisited(boolean b)
    {
        visited=b;
    }
    public boolean getVisited()
    {
        return visited;
    }
    public Edge(Node s, Node e, double w, double scaleFactor) 
    {
        
        weight=w;
        start=s;
        end=e;
         upsideDown=false;
         backwards =false;
        this.scaleFactor=scaleFactor;
        initialWeight=w/scaleFactor;
       
       
    }
    public void setColor(Color color)
    {
        myColor=color;
    }
    public Color getColor()
    {
        return myColor;
    }
    public Node getStart()
    {
        return start;
    }
    public Node getEnd()
    {
        return end;
    }
    public double getWeight()
    {
        return weight;
    }
    public void setWeight(double w)
    {
        weight=w;
    }
    
    public double getUnscaledWeight()
    {
        return initialWeight;
    }
    public boolean beenVisited()
    {
        return visited;
    }
    public void setBeenVisited(boolean b)
    {
        visited=b;
    }
    public double getAttX(double k)
{
        /*if(getAngle()>0)
        {
            return ((weight-start.getPosition().distance(end.getPosition()))*Math.sin(getAngle()));
        }
        if(start.getPosition().getX()<end.getPosition().getX())
        {
            return -1*((weight-start.getPosition().distance(end.getPosition()))*(Math.cos(getAngle())));
        }*/
        double temp=((weight-start.getPosition().distance(end.getPosition()))*k*(Math.cos(getAngle())));
        if(!backwards)
        {
            temp*=-1;
        }
        return temp;
    }
    public double getAttY(double k)
    {
       /*if(getAngle()>0)
       {
           return -1*((weight-start.getPosition().distance(end.getPosition()))*Math.cos(getAngle()));
       }*/
        /*if(start.getPosition().getY()>end.getPosition().getY())
        {
            return -1*((weight-start.getPosition().distance(end.getPosition()))*(Math.sin(getAngle())));
        }*/
        double temp= ((weight-start.getPosition().distance(end.getPosition()))*k*(Math.sin(getAngle())));
        if(upsideDown)
        {
            temp*=-1;
        }
        return temp;
    }
    public double getAttX()
{
        return getAttX(1);
    }
    public double getAttY()
    {
       return getAttY(1);
    }
    public double getAngle()
    {
        
        double angle=0;
        double y=0;
        double x=0;
        if(end.getPosition().getY()>start.getPosition().getY())
        {
            y=end.getPosition().getY()-start.getPosition().getY();
            upsideDown=true;
        }
        else
        {
            y=start.getPosition().getY()-end.getPosition().getY();
            upsideDown=false;
        }
        if(end.getPosition().getX()>start.getPosition().getX())
        {
            x=end.getPosition().getX()-start.getPosition().getX();
            backwards=false;
        }
        else
        {
            x=start.getPosition().getX()-end.getPosition().getX();
            backwards=true;
        }
        angle= Math.atan(y/x);
        
        
        //System.out.println("angle "+angle);
        return angle;
    }
    public double getLength()
    {
        return start.getPosition().distance(end.getPosition());
    }
    public void shorten(double d)
    {
        end.setPosition(new Point((int)(end.getPosition().getX()+(d*(Math.cos(getAngle())))),(int)(end.getPosition().getY()+(d*(Math.sin(getAngle()))))));
        //end.setPosition(new Point(end.getPosition().getX()-(d*(Math.cos(getAngle()))),end.getPosition().getY()-(d*(Math.sin(getAngle())))));
    }
    public boolean isBackward()
    {
        return backwards;
    }
    public boolean isUpsideDown()
    {
        return upsideDown;
    }
    public void setScale(double scale)
    {
        scaleFactor=scale;
    }
    public double getScale()
    {
        return scaleFactor;
    }
    public void printError()
    {
        System.out.println(start.getName()+"-->"+end.getName()+": Weight= "+getWeight()+" : Actual= "+getLength()+" : Diff= "+(getLength() - getWeight()));
    }
    public boolean equals(Object o)
    {
        Edge e=(Edge)o;
        return (e.getStart().getName().equals(getStart().getName())&&e.getEnd().getName().equals(getEnd().getName()));
    }
    @Override
    public int compareTo(Object t) {
        if(t instanceof Edge)
        {
            return (int)(getWeight()-((Edge)t).getWeight());
        }
        throw new ClassCastException();
    }
}
