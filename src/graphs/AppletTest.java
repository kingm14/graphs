/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

/**
 *
 * @author kingm14
 */
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

public class AppletTest extends Applet implements KeyListener
{
	private String keyPressed;

	public void init()
	{
		addKeyListener(this);
		keyPressed = "You must click in this window to begin (to give the applet focus).";
	}
	public void paint(Graphics g)
	{
		g.drawString(keyPressed, 20, 40);
	}
	public void keyPressed(KeyEvent e)
	{
		keyPressed = "You pressed the " + e.getKeyText(e.getKeyCode());
		repaint();
	}
	public void keyReleased(KeyEvent e)
	{
		keyPressed = "You released the " + e.getKeyText(e.getKeyCode());
		repaint();
	}
	public void keyTyped(KeyEvent e){}
}