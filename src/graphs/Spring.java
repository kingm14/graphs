/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

/**
 *
 * @author kingm14
 */
public class Spring {
    private double k;
    private double prefL;
    private Node s;
    private Node e;
    
    public double getPrefL ()
    {
        return prefL;
    }
    public void setPrefL (double l)
    {
        prefL=l;
    }
    public double getK ()
    {
        return k;
    }
    public void setK (double k)
    {
        this.k=k;
    }
    public Node getS ()
    {
        return s;
    }
    public void setS (Node s)
    {
        this.s=s;
    }
    public Node getE ()
    {
        return e;
    }
    public void setE (Node e)
    {
        this.e=e;
    }
}
