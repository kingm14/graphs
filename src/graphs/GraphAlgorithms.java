/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

import java.io.*;
import java.awt.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kingm14
 */
public class GraphAlgorithms {
    private HashMap<String, Color> colorMap;
    public static void main(String[] args) {
        // TODO code application logic here
        GraphAlgorithms ga=new GraphAlgorithms();
        ga.readFromFile("graph1.txt");
       
        Scanner keyboard=new Scanner(System.in);
        System.out.print("Please enter start point: ");
        String s=keyboard.nextLine();
        while(!ga.validNode(s))
        {
            System.out.print("  Invalid input, Please enter start point: ");
            s=keyboard.nextLine();
        }
        System.out.println();
        System.out.print("Please enter end point: ");
        String e=keyboard.nextLine();
        while(!ga.validNode(e))
        {
            System.out.print("  Invalid input, Please enter end point: ");
            e=keyboard.nextLine();
        }
        //ga.shortenEdges(s, e);
        
        MapDrawer m=new MapDrawer();
        m.initGraph(ga.getAllNodes());
    }
    /**
     * @param args the command line arguments
     */
    HashMap<String,Node> lookup;
    
    public GraphAlgorithms()
    {
        lookup=new HashMap<String,Node>();
        colorMap=new HashMap<String, Color>();
    }
    
    public boolean validNode(String s)
    {
        return lookup.containsKey(s);
    }
    public void readFromFile(File f)
    {
        
        int scaleFactor=1;
        readFromFile(f,1);
    }
    public void readFromFile(File f, double scaleFactor)
    {
        
        
        Scanner scan=null;
        
        try {
            scan=new Scanner(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GraphAlgorithms.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        while(scan.hasNextLine())
        {
            String temp=scan.nextLine();
            
            String[] tempL=temp.split(",");
            if(tempL.length>=4)
            {
                addNewEdge(tempL[0],tempL[1],Double.parseDouble(tempL[2])*scaleFactor,scaleFactor,tempL[3]);
            }
            else
            {
                addNewEdge(tempL[0],tempL[1],Double.parseDouble(tempL[2])*scaleFactor,scaleFactor);
            }
        }
        scan.close();
    }
    public void readFromFileL2L(String s, double scaleFactor)
    {
        File f=new File(s);
        
        Scanner scan=null;
        
        try {
            scan=new Scanner(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GraphAlgorithms.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        while(scan.hasNextLine())
        {
            String temp=scan.nextLine();
            
            String[] tempL=temp.split(",");
            
            String name1=tempL[0];
            name1=name1.substring(name1.length()-3);
            String name2=tempL[1];
            name2=name2.substring(name2.length()-3);
            addNewEdge(name1,name2,Double.parseDouble(tempL[2])*scaleFactor,scaleFactor);
            
        }
        scan.close();
    }
    public void readFromFile(String s)
    {
        File f=new File(s);
        readFromFile(f);
    }
    public void readFromFile(String s,double sf)
    {
        File f=new File(s);
        readFromFile(f,sf);
    }
    public void addNewEdge(String n1, String n2, double weight, double scaleFactor)
    {
        if(!lookup.containsKey(n1))
        {
            lookup.put(n1, new Node(n1));
        }
        if(!lookup.containsKey(n2))
        {
            lookup.put(n2, new Node(n2));
        }
        Random rand=new Random();
        Edge temp1=new Edge(lookup.get(n1), lookup.get(n2), weight,scaleFactor);
        //temp1.setColor(new Color(rand.nextInt(230)+10,rand.nextInt(230)+10,rand.nextInt(230)+10));
        temp1.setColor(Color.white);
        lookup.get(n1).addChild(temp1);
        
        Edge temp2=new Edge(lookup.get(n2), lookup.get(n1), weight,scaleFactor);
        //temp2.setColor(Color.white);
        
        temp2.setColor(new Color(rand.nextInt(230)+10,rand.nextInt(230)+10,rand.nextInt(230)+10));
        lookup.get(n2).addChild(temp2);
        
        
    }
    public void addNewEdge(String n1, String n2, double weight, double scaleFactor, String name)
    {
        
        if(!lookup.containsKey(n1))
        {
            lookup.put(n1, new Node(n1));
        }
        if(!lookup.containsKey(n2))
        {
            lookup.put(n2, new Node(n2));
        }
        Random rand=new Random();
        Edge temp1=new Edge(lookup.get(n1), lookup.get(n2), weight,scaleFactor);
        temp1.setName(name);
        //temp1.setColor(new Color(rand.nextInt(230)+10,rand.nextInt(230)+10,rand.nextInt(230)+10));
        temp1.setColor(Color.white);
        lookup.get(n1).addChild(temp1);
        
        Edge temp2=new Edge(lookup.get(n2), lookup.get(n1), weight,scaleFactor);
        temp2.setName(name);
        if(name!=null && colorMap.containsKey(name))
        {
            temp2.setColor(colorMap.get(name));
        }
        else
        {
            Color tempc=new Color(rand.nextInt(230)+10,rand.nextInt(230)+10,rand.nextInt(230)+10);
            temp2.setColor(tempc);
            colorMap.put(name, tempc);
        }
        //temp2.setColor(Color.white);
        
        lookup.get(n2).addChild(temp2);
        
        
    }
    public void addNewEdge(String n1, String n2, double weight )
    {
        if(!lookup.containsKey(n1))
        {
            lookup.put(n1, new Node(n1));
        }
        if(!lookup.containsKey(n2))
        {
            lookup.put(n2, new Node(n2));
        }
        
        Edge temp1=new Edge(lookup.get(n1), lookup.get(n2), weight);
        lookup.get(n1).addChild(temp1);
        Edge temp2=new Edge(lookup.get(n2), lookup.get(n1), weight);
        lookup.get(n2).addChild(temp2);
        
        
    }
    public void addNewEdge(String n1, String n2, double weight, String Name )
    {
        if(!lookup.containsKey(n1))
        {
            lookup.put(n1, new Node(n1));
        }
        if(!lookup.containsKey(n2))
        {
            lookup.put(n2, new Node(n2));
        }
        
        Edge temp1=new Edge(lookup.get(n1), lookup.get(n2), weight);
        lookup.get(n1).addChild(temp1);
        Edge temp2=new Edge(lookup.get(n2), lookup.get(n1), weight);
        lookup.get(n2).addChild(temp2);
        
        
    }
    
    public void shortenEdges(String s, String e)
    {
        //System.out.println("starting");
        ArrayList<String> nodes= new ArrayList<String>( lookup.keySet());
        ArrayList<Node> connected=new ArrayList<Node>();
        //initialize
        for(int i=0; i<nodes.size(); i++)
        {
            lookup.get(nodes.get(i)).setWFS(Double.MAX_VALUE);
        }
        lookup.get(s).setWFS(0);
        
        connected.add(lookup.get(s));
        
        //find min Weight Edge
        boolean atEnd=false;
        Node prev=null;
        
        Node minNode=lookup.get(s);
        
        while(!atEnd)
        {
            minNode=connected.get(0);
            //System.out.println("finding min");
            for(int i=0; i<connected.size(); i++)
            {
                if(connected.get(i).getWFS()<minNode.getWFS())
                {
                    minNode=connected.get(i);
                }
            }
            //System.out.println("found min: "+minNode.getName()+"_"+minNode.getWFS());
            if(minNode.getName().equals(e))
            {
                atEnd=true;
            }
            else{
            //set weight
                
                //System.out.println("updating children");
                ArrayList<Edge> edges=minNode.getChildren();
                for(int i=0; i< edges.size(); i++)
                {
                    if(edges.get(i).getEnd().getWFS()>(edges.get(i).getWeight()+minNode.getWFS()))
                    {
                        edges.get(i).getEnd().setWFS(edges.get(i).getWeight()+minNode.getWFS());
                        edges.get(i).getEnd().setPrevNode(minNode);
                        edges.get(i).getEnd().setPrevEdge(edges.get(i));
                        edges.get(i).getEnd().setLWeight(edges.get(i).getWeight());
                        //if(!connected.contains(edges.get(i).getEnd()))
                        connected.add(edges.get(i).getEnd());
                        
                    }
                    
                        
                }
                
                connected.remove(minNode);
                
                

                
            }
            
        }
        
        //getDirections(s,e);
        
        
        
    }
    public ArrayList<Edge> getPath(String s, String e)
    {
        ArrayList<Edge> al=new ArrayList<Edge>();
        Node cur=lookup.get(e);
        while(!cur.equals(lookup.get(s)))
        {
            
            Node temp=cur.getPrevNode();
            double d=cur.getWFS()-temp.getWFS();
            Edge ed=cur.getPrevEdge();
            al.add(ed);
            cur=temp;
            
            
        }
        
        return al;
    }
    public void getDirections(String s, String e)
    {
        Stack<Edge> stack=new Stack<Edge>();
        Node cur=lookup.get(e);
        while(!cur.equals(lookup.get(s)))
        {
            
            Node temp=cur.getPrevNode();
            double d=cur.getWFS()-temp.getWFS();
            Edge ed=new Edge(temp,cur,d);
            stack.push(ed);
            cur=temp;
            
            
        }
        
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Directions");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        while(!stack.empty())
        {
            System.out.println(stack.peek().getStart().getName());
            System.out.println(" continue for "+stack.pop().getUnscaledWeight()+" units");
        }
        System.out.println("Arrive at "+lookup.get(e).getName());
        System.out.println();
        System.out.println();
        System.out.println("Total weight: "+lookup.get(e).getWFS());
    }
    public ArrayList<Node> getAllNodes()
    {
        return new ArrayList<Node>(lookup.values());
    }
    public ArrayList<Edge> getMinSpanningTree(ArrayList<Node> allNodes)
    {
        ArrayList<Node> nodesTV= (ArrayList<Node>) allNodes.clone();
        
        ArrayList<Edge> allCEdges=new ArrayList<Edge>();
        ArrayList<Edge> minTree=new ArrayList<Edge>();
        allCEdges.addAll(nodesTV.get(0).getChildren());
        nodesTV.remove(0);
        while(nodesTV.size()>0)
        {
            Edge minE=allCEdges.get(0);
            for(Edge e:allCEdges)
            {
                if(e.getWeight()<minE.getWeight())
                {
                    minE=e;
                }
                
            }
            //System.out.println(minE.getStart().getName()+"-->"+minE.getEnd().getName());
            
            allCEdges.remove(minE);
            if(nodesTV.contains(minE.getEnd()))
            {
                
                minTree.add(minE);
                
                nodesTV.remove(minE.getEnd());
                allCEdges.addAll(minE.getEnd().getChildren());
            }
            
        }
        
        
        return minTree;
    }
    
}
